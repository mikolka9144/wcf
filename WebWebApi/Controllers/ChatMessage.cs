﻿namespace WebWebApi.Controllers
{
    public class ChatMessage
    {
        public string Text { get; set; }
        public string NickName { get; set; }
    }
}