﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebWebApi.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class kotek : ControllerBase
    {
        public static List<ChatMessage> currentMessages = new List<ChatMessage>();

        [HttpPost]
        public void SendMessage(ChatMessage message)
        {
            currentMessages.Add(message);
        }

        [HttpGet]
        public List<ChatMessage> GetAllMessages()
        {
            return currentMessages;
        }
    }
}
