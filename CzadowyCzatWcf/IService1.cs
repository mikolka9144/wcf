﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CzadowyCzatWcf
{
    // UWAGA: możesz użyć polecenia „Zmień nazwę” w menu „Refaktoryzuj”, aby zmienić nazwę interfejsu „IService1” w kodzie i pliku konfiguracji.
    [ServiceContract]
    public interface IChatService
    {

        [OperationContract]
        int GetNumberOfUsers();

        [OperationContract]
        void SendMessage(Message composite);

        [OperationContract]
        List<Message> GetMessages();

        // TODO: dodaj tutaj operacje usługi
    }


    // Użyj kontraktu danych, jak pokazano w poniższym przykładzie, aby dodać typy złożone do operacji usługi.
    [DataContract]
    public class Message
    {
        
        [DataMember]
        public User User { get; set; }
        
        [DataMember]
        public string MessageText { get; set; }
        [DataMember]
        public string Kotek { get; set; }
        
    }

    [DataContract]
    public class User
    {
        [DataMember] public string Nickname { get; set; }
    }
}
