﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace CzadowyCzatWcf
{
    // UWAGA: możesz użyć polecenia „Zmień nazwę” w menu „Refaktoryzuj”, aby zmienić nazwę klasy „Service1” w kodzie, usłudze i pliku konfiguracji.
    // UWAGA: aby uruchomić klienta testowego WCF w celu przetestowania tej usługi, wybierz plik Service1.svc lub Service1.svc.cs w eksploratorze rozwiązań i rozpocznij debugowanie.
    public class Service1 : IChatService
    {
        public static List<Message> messages=new List<Message>();
        public int GetNumberOfUsers()
        {
            return -2;
        }

        public void SendMessage(Message composite)
        {
           messages.Add(composite);
        }

        public List<Message> GetMessages()
        {
            return messages;
        }
    }
}
