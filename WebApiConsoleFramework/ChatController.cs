﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebWebApi.Controllers
{
    public class ChatController : ApiController
    {
        public static List<ChatMessage> currentMessages = new List<ChatMessage>();
        public void SendMessage(ChatMessage message)
        {
            currentMessages.Add(message);
        }

        public List<ChatMessage> GetAllMessages()
        {
            return currentMessages;
        }
    }
}
