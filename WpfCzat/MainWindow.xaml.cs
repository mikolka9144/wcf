﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfCzat.ServiceReference1;

namespace WpfCzat
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DispatcherTimer timer=new DispatcherTimer();
            timer.Interval=new TimeSpan(1000);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
           // WcfChatGetMesssages();
            HttpClient client=new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59838/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            var answer =client.GetAsync("http://localhost:59838/api/chat").Result.Content.ReadAsStringAsync().Result;
            var msgList=Newtonsoft.Json.JsonConvert.DeserializeObject<List<ChatClientMessage>>(answer);
            CzatBox.Text = "";

            foreach (var message in msgList)
            {
                if(message==null)
                    continue;
                CzatBox.Text += message.NickName + ">>" + message.Text+Environment.NewLine;
            }

        }

        private void WcfChatGetMesssages()
        {
            ChatServiceClient client = new ChatServiceClient();
            var msg = client.GetMessages();
            CzatBox.Text = "";
            foreach (var message in msg)
            {

                CzatBox.Text += message.User.Nickname + ">>" + message.MessageText;
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59838/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(new ChatClientMessage()
            {
                Text = myCzatLine.Text,
                NickName =  "Arek",
                ColorkiP = "cokolwiek"
            }); 

          
            client.PostAsync("api/chat", new StringContent(json,Encoding.UTF8,"application/json"));
        }
        public class ChatClientMessage
        {
            public string Text { get; set; }
            public string NickName { get; set; }
            public string ColorkiP { get; set; }
        }
    }
}
